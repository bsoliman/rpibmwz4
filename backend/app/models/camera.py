import io
import picamera
from threading import Condition
import threading

class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)

class Camera:

    is_on = False
    frame = None
    thread = None

    def __start_stream(self):
        with picamera.PiCamera(resolution='1920x1080', framerate=30) as camera:
            output = StreamingOutput()
            camera.rotation = 270
            camera.start_recording(output, format='mjpeg')
            while self.is_on:
                with output.condition:
                    output.condition.wait()
                    self.frame = output.frame
                

    def streamer(self):
        try:
            while True:
                if(self.frame != None):
                    yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
                        self.frame + b'\r\n')
        except GeneratorExit:
            print("cancelled")

    def start_camera(self):
        if not self.is_on:
            self.is_on = True
            self.thread = threading.Thread(target=self.__start_stream).start()

    def stop_camera(self):
        self.is_on = False
        self.frame = None
        if(self.thread != None):
            self.thread.join()
            self.thread = None