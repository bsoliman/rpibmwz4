# RPi Car project

## watchtower run
cd /watchtower
docker-compose up -d

## project run
docker-compose up -d


## put a unit file for backend
sudo cp bmwz4backend.service /etc/systemd/system/bmwz4backend.service
sudo systemctl daemon-reload
sudo systemctl enable bmwz4backend
sudo systemctl start bmwz4backend

## i2c issue solution
https://github.com/raspberrypi/linux/issues/2228

The problem is clearly triggered by the kernel taking a write followed by a read and combining them into a single transaction, allowing it to squash a stop followed by start into a restart.

There are two known bugs in the I2C controller. Both are discussed in this old issue: #254
I'm not going to attempt to summarise the conclusions or suggest a more permanent way forward until I've thoroughly grokked the thread myself.

You can permanently disable the combined trasnsactions by creating a .conf file in /etc/modprobe.d:

$ sudo sh -c "echo options i2c_bcm2708 combined=N > /etc/modprobe.d/i2c_bcm2708.conf"
However, since the driver default is for them to be disabled you must already have a conf file enabling them:

$ grep combined /etc/modprobe.d/*
If so, remove that line or delete the entire file.

### also some config to /boot/config.txt
dtparam=i2c_baudrate=100000
core_freq=250
dtoverlay=i2c-bcm2708

## command to copy frontend to caddy
sudo cp -r ../frontend/public/* /usr/share/caddy/frontend/