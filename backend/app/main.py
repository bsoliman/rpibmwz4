from fastapi import FastAPI, Query
from fastapi.responses import StreamingResponse
from typing import List

from .models.motor import DIRECTIONAL_MOTOR_STATE, MOVEMENT_MOTOR_STATE
from .models.led import LedModel
from .models.car import Car
from .models.car import CarModel

bmwz4 = Car() 

bmwz4_app = FastAPI()

@bmwz4_app.on_event("shutdown")
def close_system():
    bmwz4.stop_system()

@bmwz4_app.get("/car-states", response_model=CarModel)
def get_car_states():
    return bmwz4.get_car_model()

@bmwz4_app.put("/led", response_model=CarModel)
async def set_led_state(leds: List[LedModel]):
    bmwz4.set_light_state(leds)
    return bmwz4.get_car_model()

@bmwz4_app.get("/camera")
async def video_stream():
    return StreamingResponse(bmwz4.streamer(), media_type="multipart/x-mixed-replace;boundary=frame")

@bmwz4_app.get("/start-camera")
def start_camera():
    bmwz4.start_camera()

@bmwz4_app.get("/stop-camera")
def stop_camera():
    bmwz4.stop_camera()

@bmwz4_app.put("/movement")
def set_movement(state: MOVEMENT_MOTOR_STATE = None, speed: int = Query(None, ge=0, le=100)):
    bmwz4.set_movement_state(state, speed)

@bmwz4_app.put("/direction")
def set_direction(state: DIRECTIONAL_MOTOR_STATE = None, angle_steepness: int = Query(None, ge=0, le=25)):
    bmwz4.set_direction_state(state, angle_steepness)