import { writable } from 'svelte/store';
import {LED_STATES, CAMERA_STATES} from '../Models/constants';

export const headlightsLedState = writable(LED_STATES.OFF);
export const signalLeftLedState = writable(LED_STATES.OFF);
export const signalRightLedState = writable(LED_STATES.OFF);
export const backlightLedState = writable(LED_STATES.OFF);
export const reverselightLedState = writable(LED_STATES.OFF);

export const cameraState = writable(CAMERA_STATES.OFF);

export const onInterval = writable(0.5);
export const offInterval = writable(1);