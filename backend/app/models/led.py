from pydantic import BaseModel
from enum import Enum
import time
import RPi.GPIO as GPIO
import threading 


class LED_STATE(Enum):
    ON = 'ON'
    OFF = 'OFF'
    BLINK = 'BLINK'


class LedModel(BaseModel):
    name: str
    state: LED_STATE
    on_interval: float
    off_interval: float

class Led:
    def __init__(self, name: str, gpio: int, state: LED_STATE = LED_STATE.OFF, on_interval: float = 0, off_interval: float = 0) -> None:
        GPIO.setup(gpio, GPIO.OUT)
        self.thread = None
        self.gpio = gpio
        self.led = LedModel(name = name, state = state, on_interval = on_interval, off_interval = off_interval)
        self.set_state(state, on_interval, off_interval)

    def set_state(self, state: LED_STATE = None, on_interval: float = None, off_interval: float = None) -> None:
        if on_interval != None:
            self.led.on_interval = on_interval

        if off_interval != None:
            self.led.off_interval = off_interval

        if state == LED_STATE.ON:
            self.led.state = state
            GPIO.output(self.gpio, GPIO.HIGH)
            if(self.thread != None):
                self.thread.join()
                self.thread = None
        elif state == LED_STATE.OFF:
            self.led.state = state
            GPIO.output(self.gpio, GPIO.LOW)
            if(self.thread != None):
                self.thread.join()
                self.thread = None
        elif state == LED_STATE.BLINK and self.led.state != LED_STATE.BLINK:
            self.led.state = state
            self.thread = threading.Thread(target=self.__blink).start()

    def __blink(self):
        while self.led.state == LED_STATE.BLINK:
            GPIO.output(self.gpio, GPIO.HIGH)
            time.sleep(self.led.on_interval)
            GPIO.output(self.gpio, GPIO.LOW)
            time.sleep(self.led.off_interval)

    def get_status(self) -> LedModel:
        return self.led

    def switch_off(self):
        self.set_state(LED_STATE.OFF, 0, 0)

    def __del__(self):
        GPIO.cleanup()
           

