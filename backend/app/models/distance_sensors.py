from enum import Enum
from pydantic import BaseModel
from ..libraries.vl6180x_multi import MultiSensor


class SENSORS_GPIO(Enum):
    FRONT = 4
    BACK = 17

class DistanceSensor(BaseModel):
    name: str
    range: int

class DistanceSensors:

    def __init__(self):
        self.ms = MultiSensor([SENSORS_GPIO.FRONT.value,SENSORS_GPIO.BACK.value])
        self.front_sensor = DistanceSensor(name= SENSORS_GPIO.FRONT.name, range= self.ms.sensors[0].range)
        self.back_sensor = DistanceSensor(name= SENSORS_GPIO.BACK.name, range= self.ms.sensors[1].range)

    def get_ranges(self):
        self.front_sensor.range = self.ms.sensors[0].range
        self.back_sensor.range = self.ms.sensors[1].range
        return [self.front_sensor, self.back_sensor]
        
