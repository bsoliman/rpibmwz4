from pydantic import BaseModel
import RPi.GPIO as GPIO
from enum import Enum
import threading

from .distance_sensors import DistanceSensors

from .motor import MOVEMENT_MOTOR_STATE, Motors, CarMotors
from .led import Led
from .camera import Camera
from .imu import IMU, IMUModel

class CarModel(BaseModel):
    leds: list
    motors: CarMotors
    distance_sensors: list
    imu: IMUModel

class LEDS_GPIO(Enum):
    SIGNAL_RIGHT_FRONT = 21
    SIGNAL_RIGHT_BACK = 26
    HEADLIGHTS = 25
    STOP_LIGHT = 24
    SIGNAL_LEFT_FRONT = 20
    SIGNAL_LEFT_BACK = 6
    REVERSE_LIGHT = 16

class MOTORS_GPIO(Enum):
    FORWARD = 19
    BACKWARD = 13
    SERVO = 12

class Car:
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        self.leds = {
            LEDS_GPIO.SIGNAL_RIGHT_FRONT.name: Led(LEDS_GPIO.SIGNAL_RIGHT_FRONT.name, LEDS_GPIO.SIGNAL_RIGHT_FRONT.value),
            LEDS_GPIO.SIGNAL_LEFT_FRONT.name: Led(LEDS_GPIO.SIGNAL_LEFT_FRONT.name, LEDS_GPIO.SIGNAL_LEFT_FRONT.value),
            LEDS_GPIO.SIGNAL_RIGHT_BACK.name: Led(LEDS_GPIO.SIGNAL_RIGHT_BACK.name, LEDS_GPIO.SIGNAL_RIGHT_BACK.value),
            LEDS_GPIO.SIGNAL_LEFT_BACK.name: Led(LEDS_GPIO.SIGNAL_LEFT_BACK.name, LEDS_GPIO.SIGNAL_LEFT_BACK.value),
            LEDS_GPIO.HEADLIGHTS.name: Led(LEDS_GPIO.HEADLIGHTS.name, LEDS_GPIO.HEADLIGHTS.value),
            LEDS_GPIO.REVERSE_LIGHT.name: Led(LEDS_GPIO.REVERSE_LIGHT.name, LEDS_GPIO.REVERSE_LIGHT.value),
            LEDS_GPIO.STOP_LIGHT.name: Led(LEDS_GPIO.STOP_LIGHT.name, LEDS_GPIO.STOP_LIGHT.value)
        }
        self.camera = Camera()
        self.motors = Motors(MOTORS_GPIO.FORWARD.value, MOTORS_GPIO.BACKWARD.value, MOTORS_GPIO.SERVO.value)
        self.distance_sensors = DistanceSensors()
        self.imu = IMU()
        threading.Thread(target=self.auto_stop).start()

    def set_light_state(self, leds):
        for led_param in leds:
            led = self.leds.get(led_param.name)
            if led != None:
                led.set_state(led_param.state, led_param.on_interval, led_param.off_interval)

    def stop_system(self):
        for name, led in self.leds.items():
            led.switch_off()
        self.stop_camera()
        self.motors.switch_off()

    def get_car_model(self) -> CarModel:
        leds = []
        for name, led in self.leds.items():
            leds.append(led.led)
        return CarModel(leds = leds, motors = self.motors.carmotors, distance_sensors = self.distance_sensors.get_ranges(), imu = self.imu.get_data())

    def streamer(self):
        return self.camera.streamer()
    
    def start_camera(self):
        return self.camera.start_camera()

    def stop_camera(self):
        return self.camera.stop_camera()

    def set_movement_state(self, state, speed):
        self.motors.set_movement_state(state, speed)

    def set_direction_state(self, state, angle_steepness):
        self.motors.set_direction_state(state, angle_steepness)

    def auto_stop(self):
        while True:
            front_sensor, back_sensor = self.distance_sensors.get_ranges()
            if (front_sensor.range < 200 and self.motors.carmotors.movement_motor.state == MOVEMENT_MOTOR_STATE.FORWARD) or (back_sensor.range < 200 and self.motors.carmotors.movement_motor.state == MOVEMENT_MOTOR_STATE.BACKWARD):
                self.motors.stop_movement()