from pydantic import BaseModel
from enum import Enum
import RPi.GPIO as GPIO
from time import sleep

class MOTOR_TYPE(Enum):
    MOVEMENT = 'MOVEMENT'
    DIRECTIONAL = 'DIRECTIONAL'

class MOVEMENT_MOTOR_STATE(Enum):
    FORWARD = 'FORWARD'
    BACKWARD = 'BACKWARD'
    OFF = 'OFF'

class DIRECTIONAL_MOTOR_STATE(Enum):
    RIGHT = 'RIGHT'
    LEFT = 'LEFT'
    OFF = 'OFF'


class MovementMotorModel(BaseModel):
    state: MOVEMENT_MOTOR_STATE
    speed: int

class DirectionalMotorModel(BaseModel):
    state: DIRECTIONAL_MOTOR_STATE
    angle_steepness: int

class CarMotors(BaseModel):
    movement_motor: MovementMotorModel
    directional_motor: DirectionalMotorModel

class Motors:
    def __init__(self, gpio_forward: int, gpio_backward: int, gpio_servo: int) -> None:
        GPIO.setup(gpio_forward, GPIO.OUT)
        GPIO.setup(gpio_backward, GPIO.OUT)
        GPIO.setup(gpio_servo, GPIO.OUT)
        self.pwm_movement = None
        self.gpio_forward = gpio_forward
        self.gpio_backward = gpio_backward
        self.gpio_servo = gpio_servo
        self.carmotors = CarMotors(
            movement_motor = MovementMotorModel(state = MOVEMENT_MOTOR_STATE.OFF, speed = 0)
        , directional_motor = DirectionalMotorModel(state = DIRECTIONAL_MOTOR_STATE.OFF, angle_steepness = 0))
        self.init_servo();
        self.init_motor();

    def init_servo(self):
        self.pwm_servo = GPIO.PWM(self.gpio_servo, 50)
        self.pwm_servo.start(7.5)
        sleep(0.1)
        self.pwm_servo.ChangeDutyCycle(0)
    
    def init_motor(self):
        self.pwm_forward = GPIO.PWM(self.gpio_forward, 10000)
        self.pwm_backward = GPIO.PWM(self.gpio_backward, 10000)
        self.pwm_forward.start(0)
        self.pwm_backward.start(0)


    def set_movement_state(self, state, speed) -> None:
        if speed != None:
            self.carmotors.movement_motor.speed = speed

        if state != None:
            self.carmotors.movement_motor.state = state

        if state == MOVEMENT_MOTOR_STATE.OFF:
            self.pwm_backward.ChangeDutyCycle(0)
            self.pwm_forward.ChangeDutyCycle(0)

        elif state == MOVEMENT_MOTOR_STATE.FORWARD:
            self.pwm_backward.ChangeDutyCycle(0)
            self.pwm_forward.ChangeDutyCycle(speed)
        
        elif state == MOVEMENT_MOTOR_STATE.BACKWARD:
            self.pwm_forward.ChangeDutyCycle(0)
            self.pwm_backward.ChangeDutyCycle(speed)


    def set_direction_state(self, state, angle_steepness: int) -> None:
        
        if state == DIRECTIONAL_MOTOR_STATE.OFF:
            self.pwm_servo.ChangeDutyCycle(7.5)
            sleep(0.1)
            self.pwm_servo.ChangeDutyCycle(0)
            

        elif state == DIRECTIONAL_MOTOR_STATE.RIGHT:
            if angle_steepness != None and angle_steepness >= 0 and angle_steepness <= 25:
                self.pwm_servo.ChangeDutyCycle(0.1 * angle_steepness + 7.5)
            else:
                self.pwm_servo.ChangeDutyCycle(10)
            sleep(0.1)
            self.pwm_servo.ChangeDutyCycle(0)

        elif state == DIRECTIONAL_MOTOR_STATE.LEFT:
            if angle_steepness != None and angle_steepness >= 0 and angle_steepness <= 25:
                self.pwm_servo.ChangeDutyCycle(-0.192 * angle_steepness + 7.5)
            else:
                self.pwm_servo.ChangeDutyCycle(2.7)
            sleep(0.1)
            self.pwm_servo.ChangeDutyCycle(0)


        if angle_steepness != None:
            self.carmotors.directional_motor.angle_steepness = angle_steepness

        if state != None:
            self.carmotors.directional_motor.state = state

    def get_status(self) -> CarMotors:
        return self.carmotors

    def switch_off(self):
        self.set_direction_state(DIRECTIONAL_MOTOR_STATE.OFF, 0)
        self.set_movement_state(MOVEMENT_MOTOR_STATE.OFF , 0)

    def __del__(self):
        self.switch_off()
        GPIO.cleanup()

    def stop_movement(self):
        self.set_movement_state(MOVEMENT_MOTOR_STATE.OFF , 0)