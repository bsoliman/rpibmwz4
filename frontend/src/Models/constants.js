export const LED_STATES = Object.freeze({ON:"ON", OFF:"OFF", BLINK:"BLINK"});

export const CAMERA_STATES = Object.freeze({ON:"ON", OFF:"OFF"});

export const LED_NAMES = Object.freeze({SIGNAL_RIGHT_FRONT:"SIGNAL_RIGHT_FRONT", 
                                        SIGNAL_LEFT_FRONT:"SIGNAL_LEFT_FRONT",
                                        SIGNAL_RIGHT_BACK:"SIGNAL_RIGHT_BACK", 
                                        SIGNAL_LEFT_BACK:"SIGNAL_LEFT_BACK",
                                        HEADLIGHTS:"HEADLIGHTS", 
                                        STOP_LIGHT:"STOP_LIGHT",  
                                        REVERSE_LIGHT:"REVERSE_LIGHT"});

export const MOVEMENT_MOTOR_STATE = Object.freeze({FORWARD:"FORWARD", BACKWARD:"BACKWARD", OFF:"OFF"});

export const DIRECTIONAL_MOTOR_STATE = Object.freeze({RIGHT:"RIGHT", LEFT:"LEFT", OFF:"OFF"});