import board
import adafruit_mpu6050
from pydantic import BaseModel

class IMUModel(BaseModel):
    acceleration_X: float
    acceleration_Y: float
    acceleration_Z: float
    gyro_X: float
    gyro_Y: float
    gyro_Z: float
    temperature: float


class IMU():
    def __init__(self):
        i2c = board.I2C()
        self.mpu = adafruit_mpu6050.MPU6050(i2c)
    
    def get_data(self) -> IMUModel:
        return IMUModel(acceleration_X = self.mpu.acceleration[0], acceleration_Y = self.mpu.acceleration[1], 
                        acceleration_Z = self.mpu.acceleration[2], gyro_X = self.mpu.gyro[0], 
                        gyro_Y = self.mpu.gyro[1], gyro_Z = self.mpu.gyro[2], temperature = self.mpu.temperature)